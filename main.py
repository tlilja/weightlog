import sys
from time import localtime, strftime
import web
#web.config.debug = False
import config
import plot
import model
import forms

#########################################################################
# Globals                                                               #
#########################################################################

urls = (
    # users
    '/login'                             , 'Login'     ,
    '/logout'                            , 'Logout'    ,
    '/signup'                            , 'Signup'    ,
    # measurements
    '/'                                  , 'Index'     ,
    '/delete/(.*)'                       , 'Delete'    ,
    '/viewall/(.*)'                      , 'ViewAll'   ,
    '/view/?(.*)'                        , 'View'      ,
    '/csv/export/(.*)'                   , 'CSVExport' ,
    '/csv/import/(.*)'                   , 'CSVImport' ,
    '/(.*)/(weight|waist).(svg|png)'     , 'Images'    ,
)

app         = web.application(urls, globals())
application = app.wsgifunc()

# need this stuff to make the session handling work in debug mode
if web.config.get('_session') is None:
    store   = model.session_store()
    session = web.session.Session(app, store, initializer={'user_id': False,
                                                           'username': False,
                                                           'error': False})
    web.config._session = session
else:
    session = web.config._session

web.template.Template.globals['session'] = session
render = web.template.render('templates/')

#########################################################################
# Utilities                                                             #
#########################################################################
def check_login():
    if not session.user_id:
        raise web.seeother('/login')
    return session.user_id

def reset_error():
    m = session.error
    if m:
        session.error = False
    return m

def username_to_id(username, seeother):
    if username:
        user_id = model.users_name_to_id(username)
        if not user_id:
            raise render.error(
                'Error: user "{0}" does not exist'.format(username))
    elif session.user_id:
        user_id = session.user_id
    return user_id

#########################################################################
# User handling                                                         #
#########################################################################
class Login:
    def GET(self):
        f = forms.login()
        return render.login(f, reset_error())

    def POST(self):
        i = web.input()
        user_id = model.users_check_password(i.username, i.password)
        if not user_id:
            session.error = 'Invalid username/password'
            return web.seeother('/login')

        session.user_id  = user_id
        session.username = i.username

        return web.seeother('/view/' + session.username)

class Logout:
    def GET(self):
        session.user_id  = False
        session.username = False
        session.kill()

        return render.logout()

class Signup:
    def GET(self):
        f = forms.signup()
        return render.signup(f, reset_error())

    def POST(self):
        f = forms.signup()
        if not f.validates():
            return render.signup(f, reset_error())

        i = web.input()
        try:
            user_id = model.users_add(i)
        except model.UserNameExists:
            session.error = 'Username already exists'
            return web.seeother('/signup')

        session.user_id   = user_id
        session.username = i.username

        return web.seeother('/view/' + session.username)

#########################################################################
# Weight data handling                                                  #
#########################################################################
class Index:
    def GET(self):
        if session.user_id:
            return web.seeother('/view/' + session.username)
        else:
            return web.seeother('/login')

class Delete:
    def POST(self, target):
        user_id = check_login()

        i = web.input()
        model.measurements_delete(user_id, i.id)
        return web.seeother('/' + target + session.username)

class View:
    def get_chart(self, data):
        if len(data) > 0:
            return render.chart(plot.dict_to_js(data, 'weight'),
                                plot.dict_to_js(data, 'waist'))
        else:
            return ""

    def GET(self, username):
        user_id = username_to_id(username, '/view/')
        form = None
        if session.username == username:
            form = forms.add()

        data = model.measurements_dict(user_id, limit=10)

        return render.view(data, form, self.get_chart(data),
                           render.navbar(username))

    def POST(self, username):
        user_id = check_login()
        form = forms.add()
        if not form.validates():
            data = model.measurements_dict(user_id, limit=10)
            return render.view(data, form, self.get_chart(data),
                               render.navbar(username))

        i = web.input()
        model.measurements_add(user_id, i.weight, i.waist, i.created)

        return web.seeother('/view/' + session.username)

class ViewAll:
    def get_chart(self, data):
        if len(data) > 0:
            return render.chart_at(plot.dict_to_js(data, 'weight'),
                                   plot.dict_to_js(data, 'waist'))
        else:
            return ""

    def GET(self, username):
        user_id = username_to_id(username, '/viewall/')
        data = model.measurements_dict(user_id)

        form = None
        if session.username == username:
            form = forms.add()

        return render.viewall(data, form,
                              self.get_chart(data),
                              render.navbar(username))

class CSVExport:
    def GET(self, username):
        user_id = username_to_id(username, '/csv/')
        csv = model.measurements_csv_export(user_id,
                                            'created', 'weight', 'waist')
        return csv


class CSVImport:
    def GET(self, username):
        user_id = username_to_id(username, '/csv/')
        form = forms.csvimport()
        return render.csvimport(form, render.navbar(username))

    def POST(self, username):
        user_id = check_login()

        csv  = web.input(file={})
        data = csv['file'].value
        model.measurements_csv_import(user_id, data)
        raise web.seeother('/view/')

class Images:
    def GET(self, username, category, fmt):
        user_id = model.users_name_to_id(username)

        data = zip(*model.measurements_list(user_id, 'created', category))

        if category == "weight":
            data = plot.draw_graph(*data, span=model.recommended_bmi(user_id),
                                    fmt=fmt)
        else:
            data = plot.draw_graph(*data, line=model.recommended_waist(user_id),
                                    fmt=fmt)

        if fmt == "png":
            web.header('Content-Type', 'image/png')
        elif fmt == "svg":
            web.header('Content-Type', 'image/svg+xml')

        return data

if __name__ == "__main__":
    app.run()

# python main.py
