import sqlite3
import hashlib

import web
import config

DB = web.database(**config.DATABASE)
# enable foreign key constraints for sqlite3
DB.query('PRAGMA foreign_keys = ON')

def session_store():
    return web.session.DBStore(DB, 'sessions')

#########################################################################
# User handling                                                         #
#########################################################################
def password_hash(password, salt):
    return hashlib.sha512(password + salt).hexdigest()

class UserNameExists(Exception):
    pass

def users_add(data):
    del data['password_again']
    del data['Signup']
    data['password'] = password_hash(data['password'], data['username'])
    # XXX sqlite3 specific
    try:
        return DB.insert('users', **data)
    except sqlite3.IntegrityError:
        raise UserNameExists


def users_check_password(username, password):
    user_pw   = password_hash(password, username)
    print(user_pw)
    query = DB.select('users',
                      what='id, password',
                      where='username=$username',
                      vars=locals()).list()
    if len(query) != 1:
        return False
    if user_pw == query[0].password:
        return query[0].id

def users_is_male(user_id):
    q = DB.select('users', what='gender', where='id=$user_id', vars=locals())
    q = q.list()
    if len(q) != 1:
        # XXX error handling
        return True
    return q[0].gender == 'male'

def users_height(user_id):
    q = DB.select('users', what='height', where='id=$user_id', vars=locals())
    q = q.list()
    if len(q) != 1:
        # XXX error handling
        return True
    return float(q[0].height)


def users_name_to_id(username):
    q = DB.select('users', what='id', where='username=$username',
                  vars=locals())

    q = q.list()
    if len(q) != 1:
        # XXX error handling
        return False
    return int(q[0].id)

#########################################################################
# Weight data handling                                                  #
#########################################################################
def measurements_dict(user_id, limit=None):
    return list(DB.select('measurements', where = 'user_id = $user_id',
                          order='created DESC', limit=limit, vars=locals()))

def measurements_list(user_id, *keys):
    result = []
    for row in measurements_dict(user_id):
        result.append(tuple([row[item] for item in keys]))
    return result

def tuple2str(t):
    return ";".join(map(str,list(t)))

def measurements_csv_export(user_id, *keys):
    result = tuple2str(keys) + "\n"
    for item in measurements_list(user_id, *keys):
        result += tuple2str(item) + "\n"
    return result

def measurements_csv_import(user_id, data):
    for line in data.splitlines():
        ts, weight, waist = line.split(';')
        measurements_add(user_id, weight, waist, ts)

def measurements_add(user_id, weight, waist, created):
    return DB.insert('measurements', user_id=user_id, weight=weight,
                     waist=waist, created=created)

def measurements_delete(user_id, id):
    return DB.delete('measurements', where='user_id = $user_id AND id = $id',
                     vars=locals())


#########################################################################
# Recommended values                                                    #
#########################################################################

def recommended_bmi(user_id):
    h = users_height(user_id)/100.0
    low  = 18.5 * h*h
    high = 25.0 * h*h
    return [low, high]

def recommended_waist(user_id):
    if users_is_male(user_id):
        return 94
    else:
        # female
        return 80

