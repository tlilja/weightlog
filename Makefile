DEPLOY_HOST = lucid.fi
DEPLOY_DIR  = weightlog/
DB_NAME     = weightlog.db
all: $(DB_NAME)

%.db: %.sql
	sqlite3 $@ <$<

deploy:
	rsync -avP --exclude=$(DB_NAME) --exclude '*.pyc' --exclude=.git . \
	 	$(DEPLOY_HOST):$(DEPLOY_DIR)/

db-deploy: deploy
	@echo -n "*** WARNING: THIS WILL ERASE THE DATABASE. PROCEED (YES/NO)? "
	@read inp && test  "$$inp" = "YES"
	ssh $(DEPLOY_HOST) "cd $(DEPLOY_DIR) && make db-backup clean all"

db-backup:
	mv $(DB_NAME) $(DB_NAME).$(shell date "+%Y%M%d-%H%M%S")

clean:
	rm -f weightlog.db

.PHONY: clean deploy
