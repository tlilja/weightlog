CREATE TABLE users
(
        id         integer        PRIMARY KEY AUTOINCREMENT         ,
        username   varchar(80)    UNIQUE NOT NULL                   ,
        password   character(128) NOT NULL                          ,
        gender     char(1)        NOT NULL                          ,
        birth_year integer        NOT NULL                          ,
        height     integer        NOT NULL                          ,
        activity   char(10)       NOT NULL

);

CREATE TABLE sessions
 (
        session_id char(128)      UNIQUE NOT NULL                   ,
        atime      timestamp      NOT NULL default current_timestamp,
        data       text
);

CREATE TABLE measurements
(
        id         integer        PRIMARY KEY                       ,
        user_id    integer        NOT NULL                          ,
        weight     integer                                          ,
        waist      integer                                          ,
        created    datetime                                         ,
        FOREIGN KEY(user_id)      REFERENCES users(id)
);
