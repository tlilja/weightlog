import config
import web
from time import localtime, strftime

from web import form

login = form.Form(
    form.Textbox('username',
                 description='Username'),
    form.Password('password',
                  description='Password'),
    form.Button('Login'),
    )

CHECK_DIGITS_COMMA = form.regexp('^[.0-9]+$', 'Only digits and comma allowed')
CHECK_TIMESTAMP    = form.regexp('[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}',
                                 'Valid format is YYYY-MM-DDTHH:MM')
signup = form.Form(
    form.Textbox('username',
                 form.notnull,
                 class_="form-control",
                 description='Username'),
    form.Password('password',
                  form.notnull,
                 class_="form-control",
                  description='Password'),
    form.Password('password_again',
                  form.notnull,
                 class_="form-control",
                  description='Password'),
    form.Radio('gender',
               ['female', 'male'],
               description='Gender'),
    form.Textbox('birth_year',
                 form.notnull,
                 form.regexp('^\d\d\d\d$',
                             'Must be a 4-digit year (e.g. 1981)'),
                 class_="form-control",
                 description='Birth Year (YYYY)'),
    form.Textbox('height',
                 form.notnull,
                 CHECK_DIGITS_COMMA,
                 class_="form-control",
                 description='Height (cm)'),
    form.Dropdown('activity',
                  [('bed_rest'    , 'Bed rest'),
                   ('sedentary'   , 'Sedentary'),
                   ('active'      , 'Active'),
                   ('very_active' , 'Very active')],
                  description='Activity'),
    form.Button('Signup',
                class_="btn btn-lg btn-primary btn-block"),
    validators = [form.Validator("Passwords didn't match.",
                                 lambda i: i.password == i.password_again)]
    )

class DateTimeLocal(form.Input):
    def get_type(self):
        return 'datetime-local'
    def render(self):
        self.attrs['value'] = strftime(config.TIMEFMT, localtime())
        return super(DateTimeLocal, self).render()

add = form.Form(
    form.Textbox('weight',
                 form.notnull,
                 CHECK_DIGITS_COMMA,
                 class_="form-control",
                 description='Weight'),
    form.Textbox('waist',
                 form.notnull,
                 CHECK_DIGITS_COMMA,
                 class_="form-control",
                 description='Waist'),
    DateTimeLocal('created',
                  form.notnull,
                  CHECK_TIMESTAMP,
                  type="datetime-local",
                  class_="form-control",
                  description='Date'),
    form.Button('Add',
                class_="btn btn-lg btn-primary btn-block"),

    )

csvimport = form.Form(
    form.File('Filename', class_="filestyle"),
    form.Button('Send',
                class_="btn btn-lg btn-primary btn-block"),
    )
