import cStringIO
import datetime as DT
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.dates import date2num
from matplotlib.dates import YearLocator, MonthLocator, DateFormatter

def date_to_js(date):
    t = DT.datetime.strptime(date, '%Y-%m-%dT%H:%M')
    s = "new Date({0},{1},{2},{3},{4},0 ,0)".format(t.year,
                                               t.month - 1,
                                               t.day,
                                               t.hour,
                                               t.minute)
    return s

def dict_to_js(data, key):
    s = ""
    for item in data:
        s += "[{0},{1}],".format(date_to_js(item['created']), item[key])
    return s

def dates_to_nums(t):
    dates  = map(lambda x: DT.datetime.strptime(x, '%Y-%m-%dT%H:%M'), t)
    xt     = map(date2num, dates)
    return xt

def draw_graph(tdata, ydata, tlim=False, span=False, line=False, fmt="png"):
    fig    = Figure(figsize=[8,5])
    ax     = fig.add_axes([.12,.15,.85,.80])
    xdata  = dates_to_nums(tdata)
    canvas = FigureCanvasAgg(fig)

    ax.plot_date(xdata, ydata, '-o')
    fig.autofmt_xdate()
    ax.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
    ax.grid()
    ax.margins(.35)
    if tlim:
        xlim = dates_to_nums(tlim)
        ax.set_xlim(xlim)

    if span:
        ax.axhspan(span[0], span[1], facecolor='g', alpha=0.3)

    if line:
        ax.axhline(y=line, color='g')

    # write image data to a string buffer and get the PNG image bytes
    buf = cStringIO.StringIO()
    if fmt == "png":
        canvas.print_png(buf)
    elif fmt == "svg":
        canvas.print_svg(buf)
    data = buf.getvalue()
    return data

if __name__ == "__main__":
    d = draw_graph([u'2013-12-14T17:40',
                    u'2013-12-15T17:45',
                    u'2013-12-16T17:41'],
                   [1, 3, 2],
                   tlim=['2013-12-01T15:45', '2013-12-30T18:45'],
                   span=[1.5,2.5],
                   line=1.7
                   )

    print(d)

